This app is setup to use a MySQL database, redis cache and ability to send emails. The database credentials are stored in `credentials.txt`. phpMyAdmin access is stored in `phpmyadmin_login.txt`.

 
The code and credential files can be accessed via [SFTP](https://cloudron.io/documentation/apps/#ftp-access) or the [Web Terminal](https://cloudron.io/documentation/apps/#web-terminal).
